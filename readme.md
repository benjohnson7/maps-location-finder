WWW Dev Package
===============

Set-up
------

- git clone https://gitlab.com/benjohnson7/www-dev.git
- npm install
- gulp serve
- Edit sass files to style. If you directly edit the CSS, it will be overwritten.