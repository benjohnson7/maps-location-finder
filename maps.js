var map;

var myPos;

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 5,
		center: new google.maps.LatLng(39.458511, -95.549143),
		mapTypeId: 'terrain'
	});
	var icon = {
		icon: 'rusty-taco-logo.png',
	}
	for (var o = 0; o < locations.length; o++){
		locations[o].coords = new google.maps.LatLng(locations[o].coords[0], locations[o].coords[1]);
	}

	// SEARCH
	initSearch()
	// END SEARCH

	// GEO LOCATION
	// initGeo()
	tryGeolocation();
	// END GEO LOCATION

	generateLocations(locations);

	
}


function apiGeolocationSuccess(position) {
	console.log("API geolocation success!\n\nlat = " + position.coords.latitude + "\nlng = " + position.coords.longitude);
};

function tryAPIGeolocation() {
	jQuery.post("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDKe2xatyzu5MEtjXU0zU80sykss2FO6HM", function(success) {
			myPos = new google.maps.LatLng(success.location.lat, success.location.lng)
			initGeo();
		})
		.fail(function(err) {
			console.log("API Geolocation error! \n\n" + err);
		});
};

function browserGeolocationSuccess(position) {
	myPos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
	initGeo()
};

function browserGeolocationFail(error) {
	switch (error.code) {
		case 3:
			console.log("Browser geolocation error !\n\nTimeout.");
			break;
		case 1:
			if (error.message.indexOf("User denied geolocation prompt") == 0) {
				tryAPIGeolocation();
			}
			break;
		case 2:
			console.log("Browser geolocation error !\n\nPosition unavailable.");
			break;
	}
};

function tryGeolocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(
			browserGeolocationSuccess,
			browserGeolocationFail, { maximumAge: 50000, timeout: 20000 });
	}
};

function renderLocationList(locations) {
	var list = document.getElementById('locList')

	var radius = document.getElementById('radius').value

	getLocationsFromRadius(locations, radius, function(nearbyLocations){
		while (list.hasChildNodes()) {
			list.removeChild(list.lastChild);
		}
		for (var k = 0; k < nearbyLocations.length; k++) {
			var loc = document.createElement('li')
			var title = document.createElement('h3')
			var address = document.createElement('p')
			var distance = document.createElement('p')
			title.innerHTML = nearbyLocations[k].displayName
			address.innerHTML = nearbyLocations[k].address
			distance.innerHTML = Math.round(nearbyLocations[k].distance * 10 / 1609.344) / 10 + ' miles | ' + Math.round(nearbyLocations[k].duration / 60) + ' minute drive' + '<br><a style="color: #b98421; font-weight: 500;" href="/' + nearbyLocations[k].state + '/' + nearbyLocations[k].url + '/">Website</a>  | ' + '<a style="color: #b98421; font-weight: 500;" target="_blank" href="https://www.google.com/maps/dir/Current+Location/' + nearbyLocations[k].coords.lat() + ',' + nearbyLocations[k].coords.lng() + '/">Directions</a>'
			loc.appendChild(title)
			loc.appendChild(address)
			loc.appendChild(distance)
			list.appendChild(loc)
		}
		if (nearbyLocations.length <= 0){
			var loc = document.createElement('li')
			var title = document.createElement('h3')
			title.innerHTML = "Sorry! No locations found within that radius.";
			loc.appendChild(title)
			list.appendChild(loc)
		}
	});
	// for (var l = 0; l < list.childNodes.length; l++) {
	// 	list.removeChild(list.lastChild)
	// }
	// l = 0;
}



function getLocationsFromRadius(locations, radius, callback) {
	var locArray = []
	var destArray = []
	var destStr = ""
	for (var j = 0; j < locations.length; j++) {
		var distance = google.maps.geometry.spherical.computeDistanceBetween(myPos, locations[j].coords) / 1609.344
		if (distance < radius){
			destStr += locations[j].coords[0] + '%2C' + locations[j].coords[1] + '%7C'
			// locations[j].distance = distance
			locArray.push(locations[j])
			destArray.push(locations[j].coords)
			// locArray.sort(function(a, b) {
			// 	return a.distance - b.distance
			// })
		}
	}
	var distanceService = new google.maps.DistanceMatrixService()

	// https://developers.google.com/maps/documentation/javascript/distancematrix
	// convert locations coords to latlng instead of array in order for this to work

	distanceService.getDistanceMatrix(
	{
		origins: [myPos],
		destinations: destArray,
		travelMode: 'DRIVING'
	}, function(response, status) {
		if (response.rows.length === 1){
			for (var n = 0; n < response.rows[0].elements.length; n++) {
				locArray[n].distance = response.rows[0].elements[n].distance.value
				locArray[n].duration = response.rows[0].elements[n].duration.value
			}	
		}
		locArray.sort(function(a, b) {
			return a.distance - b.distance
		})
		callback(locArray)
	});

	// var queryStr = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' + myPos.lat() + ',' + myPos.lng() + '&destinations=' + destStr
	// console.log(destStr)
	// if (destStr != "") {
	// 	jQuery.post(queryStr, function(success) {
	// 		console.log(sucess)
	// 	})
	// 	.fail(function(err) {
	// 		console.log("Distance Matrix Error! \n\n" + err);
	// 	});
	// }
};

function initSearch() {
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
	});
	searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}
		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
			if (!place.geometry) {
				console.log("Returned place contains no geometry");
				return;
			}
			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
		});
		map.fitBounds(bounds)
		myPos = bounds.getCenter()
		initGeo()
	});
}

var myPosMarker;

function initGeo() {
	if (myPos) {
		if(myPosMarker){
			myPosMarker.setMap(null)
		}
		map.setCenter(myPos);
		map.setZoom(9);
		myPosMarker = new google.maps.Marker({
			position: myPos,
			map: map,
			animation: google.maps.Animation.DROP
		})
		renderLocationList(locations);
	} else {
		// Browser doesn't support Geolocation
		handleLocationError(false, infoWindow, map.getCenter());
	}
}

function bounce(marker) {
	marker.setAnimation(google.maps.Animation.BOUNCE);
	setTimeout(function() {
		marker.setAnimation(null);
	}, 750);
}

function generateLocations(x) {
	var icn = {
		url: 'rusty-taco-logo.png',
		scaledSize: new google.maps.Size(40, 40),
	}

	for (var i = 0; i < x.length; i++) {
		var marker = new google.maps.Marker({
			position: x[i].coords,
			map: map,
			icon: icn
		});
		// infowindow.setContent('<div><strong>' + x[i].displayName + '</strong><br>');
		var service = new google.maps.places.PlacesService(map);
		addInfo(marker, x[i])
	}

}

function addInfo(marker, info) {
	var contentStr;
	if (info.comingSoon === true) {
		contentStr = '<div><strong>' + info.displayName + ' - COMING SOON</strong>'
	} else if (info.comingSoon === false) {
		contentStr = '<div><strong>' + info.displayName + '</strong>'
	};
	contentStr += '<br>Address: ' + info.address + '<br>Phone #: ' + info.phone + '<br><a style="color: #b98421; font-weight: 500;" href="/' + info.state + '/' + info.url + '/">Website</a>'
	contentStr += ' | <a style="color: #b98421; font-weight: 500;" target="_blank" href="https://www.google.com/maps/dir/Current+Location/' + info.coords[0] + ',' + info.coords[1] + '/">Directions</a>'
	var infowindow = new google.maps.InfoWindow({
		content: contentStr,
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map, marker);
		bounce(marker);
	});
}
document.getElementById('radius').addEventListener('change', function(){renderLocationList(locations)});